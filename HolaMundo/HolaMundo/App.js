/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, View, StatusBar, Text } from 'react-native';

import Routes from './src/Routes';
// import Login from './src/pages/Login';
// import Signup from './src/pages/Signup';

//DIMAS - Inicia SQLite
// var SQLite = require('react-native-sqlite-storage');
// var db = SQLite.openDatabase({name: 'test.sqlite', createFromLocation: '~repsaDB.sqlite'});
//DIMAS - Finaliza SQLite

type Props = {};
export default class App extends Component<Props> {

  // constructor(props){
  //   super(props)

  //   this.state = {
  //     nombreUsuario: "",
  //   };

  //   db.transaction((tx) => {
  //     tx.executeSql('SELECT * FROM Usuarios WHERE UsuarioID=?', [2], (tx, results) => {
  //       var len = results.rows.length;
  //       if(len > 0){
  //           var row = results.rows.item(0);
  //           this.setState({nombreUsuario: row.Nombre});
  //       }
  //     })
  //   })
  // }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
            backgroundColor="#1c313a"
            barStyle="light-content">
        </StatusBar>
        <Routes/>
        {/* <Login/> */}
        {/* <Signup/> */}
        {/* <Text>{'El usuario es ' + this.state.nombreUsuario}</Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#002031',
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
  }
});
