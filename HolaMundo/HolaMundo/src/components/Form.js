import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity
} from 'react-native';

import {Actions} from 'react-native-router-flux';

var SQLite = require('react-native-sqlite-storage');
var db = SQLite.openDatabase({name: 'test.sqlite', createFromLocation: '~repsaDB.sqlite'});

export default class Form extends Component<{}> {

    constructor(props){
        super(props);
        this.state={
            Usuario:'',
            Clave:'',
        }
    }

    validarUsuario=()=>{
        const {Usuario, Clave} = this.state;
        
        db.transaction((tx) => {
            tx.executeSql('SELECT * FROM Usuarios WHERE Nombre=? AND Clave=?', [Usuario.toUpperCase(), Clave.toUpperCase()], (tx, results) => {
                var len = results.rows.length;
                //alert(len);
                if(len > 0){
                    //alert('Ok');
                    Actions.replicacion();
                } else {
                    alert('Usuario no existe');
                }
            })
        })
    };

    render(){
        return(
            <View style={StyleSheet.container}>
                <TextInput 
                    placeholder="Usuario" 
                    style={styles.inputBox}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    placeholderTextColor="#ffffff"
                    selectionColor="#fff"
                    onSubmitEditing={()=>this.password.focus()}
                    onChangeText={Usuario => this.setState({Usuario})}>
                </TextInput> 
                <TextInput 
                    placeholder="Clave" 
                    secureTextEntry={true}
                    style={styles.inputBox}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    placeholderTextColor="#ffffff"
                    ref={(input)=>this.password = input}
                    onChangeText={Clave => this.setState({Clave})}>
                </TextInput>
                <TouchableOpacity style={styles.button} onPress={this.validarUsuario}>
                    <Text style={styles.buttonText}>{this.props.type}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputBox: {
        width: 300,
        textAlign: 'left',
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 18,
        color: '#ffffff',
        marginVertical: 12,
      },
    buttonText: {
        fontSize: 18,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 12,
        paddingVertical: 13,
    }
})