// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  */

// import React, { Component } from 'react';
// import { StyleSheet, View, StatusBar, ListView, Text } from 'react-native';

// var SQLite = require('react-native-sqlite-storage');
// var db = SQLite.openDatabase({name: 'test.sqlite', createFromLocation: '~repsaDB.sqlite'});

// type Props = {};
// export default class ListarUsuarios extends Component<Props> {
//   render() {
//     const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

//     var temp = [];
//     db.executeSql("SELECT Nombre FROM Usuarios", [], function(results) {
//       var len = results.rows.length;
//       for (let i = 0; i < len; i++) {
//         temp.push(results.rows.item(i));
//       }
//       this.setState({
//           dataSource: this.state.dataSource.cloneWithRows(temp),
//       });
//     });

//     this.state = {
//       dataSource: ds.cloneWithRows([]),
//     };

//     return (
//       <View style={styles.container}>
//         <StatusBar
//             backgroundColor="#1c313a"
//             barStyle="light-content">
//         </StatusBar >
//         <ListView dataSource={this.state.dataSource}
//         renderRow={(rowData) => <Text>{rowData}</Text>}>
          
//         </ListView>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: '#002031',
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   buttonText: {
//     fontSize: 18,
//     fontWeight: '500',
//     color: '#ffffff',
//     textAlign: 'center',
//   },
//   button: {
//       width: 300,
//       backgroundColor: '#1c313a',
//       borderRadius: 25,
//       marginVertical: 12,
//       paddingVertical: 13,
//   }
// });
//FIN PRIMERO

//INICIA SEGUNDO
import React, { Component } from "react";
import { AppRegistry,
    StyleSheet,
    Text,
    View,
    Button,
    ListView,
    TouchableHighlight,
  Alert, FlatList, ScrollView} from "react-native";

var SQLite = require('react-native-sqlite-storage');
var db = SQLite.openDatabase({name: 'test.sqlite', createFromLocation: '~repsaDB.sqlite'});

export default class ListarUsuarios extends Component {
  constructor(props) {
    super(props);

    var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
        dataSource: ds.cloneWithRows([])
    };

    var self = this;
    var temp = [];
    db.executeSql("SELECT Nombre FROM Usuarios", [], function(results) {
      var len = results.rows.length;
      
      for (let i = 0; i < len; i++) {
        let row = results.rows.item(i);
        //alert(row.Nombre);
        //this.setState((prevState) => ({ record : prevState.record.push(row.Nombre)}))
        //self.state = { FlatListItems: [ { key: "Skptricks" },]};
        temp.push(row.Nombre);
        //alert("dimas");
      }
    });
    
    this.setState({
      dataSource: self.state.dataSource.cloneWithRows(temp),
    });

    // this.state = {
    //   FlatListItems: [
    //     { key: "Skptricks" },
    //     { key: "Sumit" },
    //     { key: "Amit" },
    //     { key: "React" },
    //     { key: "React Native" },
    //     { key: "Java" },
    //     { key: "Javascript" },
    //     { key: "PHP" },
    //     { key: "AJAX" },
    //     { key: "Android" },
    //     { key: "Selenium" },
    //     { key: "HTML" },
    //     { key: "Database" },
    //     { key: "MYSQL" },
    //     { key: "SQLLite" },
    //     { key: "Web Technology" },
    //     { key: "CSS" },
    //     { key: "Python" },
    //     { key: "Linux" },
    //     { key: "Kotlin" },
    //   ]
    // };
  }

  FlatListItemSeparator = () => {
    return (
      <View style={{ height: 1, width: "100%", backgroundColor: "#607D8B" }} />
    );
  };

  GetItem(item) {
    Alert.alert(item);
  }

  render() {
     return (
       <View style={styles.container}>
         {/* <FlatList
            data={ this.state.FlatListItems }
            ItemSeparatorComponent = {this.FlatListItemSeparator}
            renderItem={({item}) => <Text style={styles.item} onPress={this.GetItem.bind(this, item.key)} > {item.key} </Text>}
         /> */}
          {/* <FlatList data={this.state.record}
                    keyExtractor={(x,i) => 1}
                    renderItem={ ({item}) =>
                        <ListItem><Text>{item.Nombre}</Text></ListItem>
                    }
          /> */}
          <ScrollView>
          <ListView style={styles.container} dataSource={this.state.dataSource} renderRow={(rowData) => <Text>{rowData}</Text>} />
          </ScrollView>
          
       </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#e5e5e5"
  },
  headerText: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    fontWeight: "bold"
  },
  item: {
    padding: 10,
    fontSize: 20,
    height: 45,
  }
});
//FIN SEGUNDO

//INICIA TERCERO
// import React, { Component } from 'react';
// import { Text,
//          View,
//          Button,FlatList,ListItem,
//          TextInput,Alert,
//          StyleSheet } from 'react-native';

// // var SQLite = require('react-native-sqlite-storage')
// // var db = SQLite.openDatabase({name: 'sqliteexample.db', createFromLocation: '~sqliteexample.db'})

// var SQLite = require('react-native-sqlite-storage');
// var db = SQLite.openDatabase({name: 'test.sqlite', createFromLocation: '~repsaDB.sqlite'});
// const count = 0;

// export default class HelloWorldApp extends Component<{}> {

//   constructor(props){
//       super(props)

//       this.state = {
//         UsuarioID:'',
//         Nombre:'',
//         record:[],
//         data:{UsuarioID:'',Nombre:''},
//         dataSource: [{UsuarioID:'1',Nombre:'a1'},{UsuarioID:'1',Nombre:'a1'},{UsuarioID:'1',Nombre:'a1'}],
//       };

//       db.transaction(function (txn) {
//         txn.executeSql('SELECT * FROM Usuarios', [], function (tx, res) {

//           var len = res.rows.length;
//           var data = [];
//           //console.log('mylength: ' + len);
     
//           for (let i = 0; i < len; i++) {
//             //console.log('mylength: ' + res.rows.item(i).name);
//             data.push(res.rows.item(i).Nombre,  res.rows.item(i).UsuarioID);
//             //alert(res.rows.item(i).Nombre);
//           }
//           //alert('llega');
//           this.state({dataSource: data});
//           alert('llega');
//         });
//       });
//   }

//     // onPressInsert (){
//     //   var uid = this.state.UsuarioID;
//     //   var uname = this.state.Nombre;
//     //   console.log('lengths I ' + uid + ' abc ' + uname);
//     //   db.transaction(function (txn) {
//     //    txn.executeSql('INSERT INTO Users (Nombre,UsuarioID) VALUES (:Nombre,:UsuarioID)', [uname,uid]);
//     //    txn.executeSql('SELECT * FROM Users', [], function (tx, res) {
//     //      for (let i = 0; i < res.rows.length; ++i) {
//     //           let row = res.rows.item(i);
//     //           console.log('Select Id a : ' + dataSource.length);
//     //       }
//     //       Alert.alert('Insert Id: ' + uid + ' Name: ' + uname + ' successfully');
//     //    });
//     //   });
//     // }

//     // onPressUpdate () {
//     //   var uid = this.state.UsuarioID;
//     //   var uname = this.state.Nombre;
//     //   console.log('lengths U ' + uid + ' abc ' + uname);
//     //   db.transaction(function (txn) {
//     //     txn.executeSql('UPDATE `Users` SET  Nombre=? WHERE UsuarioID=?', [uname,'3']);
//     //     txn.executeSql('SELECT * FROM Users', [], function (tx, res) {
//     //       Alert.alert('Update Id: ' + uid + ' Name: ' + uname + ' successfully');
//     //    });
//     //   });
//     // }

//     // onPressDelete () {
//     //   var uid = this.state.UsuarioID;
//     //   var uname = this.state.Nombre;
//     //   console.log('lengths D ' + uid + ' abc ');
//     //   db.transaction(function (txn) {
//     //    txn.executeSql('DELETE FROM `Users` WHERE492 `UsuarioID`=?', [uid]);
//     //    txn.executeSql('SELECT * FROM Users', [], function (tx, res) {
//     //      Alert.alert('Delete Id: ' + uid + ' Name: ' + uname + ' successfully');
//     //    });
//     //   });
//     // }

//     onPressSelect () {
//       db.transaction(function (txn) {
//        txn.executeSql('SELECT * FROM Usuarios', [], function (tx, res) {
//          for (let i = 0; i < res.rows.length; ++i) {
//               Alert.alert('Select Id: ' + uid + ' Name: ' + uname + ' successfully');
//           }
//        });
//       });
//     }

//   render() {

//     return (
//       <View>
//       <TextInput
//          style={styles.welcome}
//          placeholder="Enter Id"
//          editable = {true}
//          value = {this.state.UsuarioID}
//          onChangeText={(text) => this.setState({UsuarioID:text})}
//          maxLength = {30}
//        />
//        <TextInput
//                style={styles.welcome}
//                value = {this.state.Nombre}
//                placeholder="Enter Full Name"
//                editable = {true}
//                onChangeText={(text) => this.setState({Nombre:text})}
//                maxLength = {30}
//              />
//          <View>
//          {/* <Button
//          onPress={this.onPressInsert.bind(this)}
//          title="Insert"
//          color="#841584"/> */}
//          {/* <Button
//          onPress={this.onPressUpdate.bind(this)}
//          title="Update"
//          color="#841584"/> */}
//          {/* <Button
//          onPress={this.onPressDelete.bind(this)}
//          title="Delete"
//          color="#841584"/> */}
//          </View>
//          <FlatList
//              data={this.state.dataSource}
//              renderItem={({item}) => <Text> Id is :: {item.UsuarioID} Name is :: {item.Nombre}</Text>}
//              keyExtractor={(item, index) => index+''}
//             />

//             <FlatList data={this.state.record}
//                          keyExtractor={(x,i) => 1}
//                          renderItem={ ({item}) =>
//                              <ListItem><Text>{item.Nombre}</Text></ListItem>
//                          }
//                />
//        </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     marginTop: 20,
//     flexDirection: 'column',
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
//  itemBlock: {
//    flexDirection: 'row',
//    paddingBottom: 5,
//  },
//  itemImage: {
//    width: 50,
//    height: 50,
//    borderRadius: 25,
//  },
//  itemMeta: {
//    marginLeft: 10,
//    justifyContent: 'center',
//  },
//  itemName: {
//    fontSize: 20,
//  },
//  itemLastMessage: {
//    fontSize: 14,
//    color: "#111",
//  }
// });
//FIN TERCERO