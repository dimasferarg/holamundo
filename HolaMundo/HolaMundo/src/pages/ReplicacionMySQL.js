/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, View, StatusBar, Text, TouchableOpacity } from 'react-native';

import {Actions} from 'react-native-router-flux';

var SQLite = require('react-native-sqlite-storage');
var db = SQLite.openDatabase({name: 'test.sqlite', createFromLocation: '~repsaDB.sqlite'});

type Props = {};
export default class ReplicacionMySQL extends Component<Props> {

  replicarUsuarios=()=>{
    //alert('Ok');

    this.state = {
      loading: true,
    };

    fetch('http://www.servirepuestos.com.sv/d9yilbtfUM85.php/welcome/y9hoS6NwZe', {
      method: 'GET'
    })
    .then((response) => response.json())
    .then((responseJson) => {
      //alert(JSON.stringify(responseJson));
      var count = Object.keys(responseJson).length;
      db.transaction((tx) => {
        tx.executeSql('DELETE FROM Usuarios', []);
      })

      for(let i = 0; i <= count; i++){
        db.transaction((tx) => {
          tx.executeSql( "INSERT INTO Usuarios (UsuarioID,Nombre,Clave) VALUES ("+responseJson[i].UsuarioID+",'"+responseJson[i].Nombre+"','"+responseJson[i].Clave+"');");
        });
      }
    })
    .catch((error) => {
      alert('Error en el proceso de migración de usuarios');
    })

    this.state = {
      loading: false,
    };
    alert('Migración de usuarios exitosa');
  }

  listarUsuarios=()=>{
    Actions.listarUsuarios();
  }

  replicarClientes=()=>{
    //alert('Ok');

    this.state = {
      loading: true,
    };

    fetch('http://www.servirepuestos.com.sv/d9yilbtfUM85.php/welcome/dDacIed4vi', {
      method: 'GET'
    })
    .then((response) => response.json())
    .then((responseJson) => {
      //alert(JSON.stringify(responseJson));
      var count = Object.keys(responseJson).length;
      db.transaction((tx) => {
        tx.executeSql('DELETE FROM Clientes', []);
      })

      for(let i = 0; i <= count; i++){
        db.transaction((tx) => {
          tx.executeSql( "INSERT INTO Clientes (PLUCliente,Codigo,Nombre) VALUES ("+responseJson[i].PLUCliente+","+responseJson[i].Codigo+",'"+responseJson[i].Nombre+"');");
        });
      }
    })
    .catch((error) => {
      alert('Error en el proceso de migración de clientes');
    })

    this.state = {
      loading: false,
    };
    alert('Migración de clientes exitosa');
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
            backgroundColor="#1c313a"
            barStyle="light-content">
        </StatusBar>
        <TouchableOpacity style={styles.button} onPress={this.replicarUsuarios}>
            <Text style={styles.buttonText}>Usuarios</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={this.replicarClientes}>
            <Text style={styles.buttonText}>Clientes</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={this.replicarDivisiones}>
            <Text style={styles.buttonText}>Divisiones</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Gestores</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Productos</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={this.listarUsuarios}>
            <Text style={styles.buttonText}>Listar Usuarios</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#002031',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
  button: {
      width: 300,
      backgroundColor: '#1c313a',
      borderRadius: 25,
      marginVertical: 12,
      paddingVertical: 13,
  }
});