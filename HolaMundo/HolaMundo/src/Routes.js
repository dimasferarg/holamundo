import React, { Component } from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';

import Login from './pages/Login';
// import Signup from './pages/Signup';
import ReplicacionMySQL from './pages/ReplicacionMySQL';
import ListarUsuarios from './pages/ListarUsuarios';

export default class Routes extends Component<{}>{
    render(){
        return(
            <Router>
                <Stack key="root" hideNavBar={true}>
                    <Scene key="login" component={Login} title="Login" initial={true} />
                    {/* <Scene key="signup" component={Signup} title="Register" /> */}
                    <Scene key="replicacion" component={ReplicacionMySQL} title="Replicacion" />
                    <Scene key="listarUsuarios" component={ListarUsuarios} title="listarUsuarios" />
                </Stack>
            </Router>
        )
    }
}